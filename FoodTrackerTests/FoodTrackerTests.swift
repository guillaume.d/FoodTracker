//
//  FoodTrackerTests.swift
//  FoodTrackerTests
//
//  Created by Guillaume Dadouche on 24/10/2016.
//  Copyright © 2016 projet tuto. All rights reserved.
//

import XCTest
@testable import FoodTracker

class FoodTrackerTests: XCTestCase {
    
    //MARK: FoodTracker Tests
    
    // Tests to confirm that the Meal initializer returns when no name or a negative rating is provided.
    func testMealInitialization() {
        
        //success
        
        let potentialItem = Meal(name: "tarteTomate", photo: nil, rating: 5)
        XCTAssertNotNil(potentialItem)
        
        
        // Failure cases.
        let noName = Meal(name: "", photo: nil, rating: 0)
        XCTAssertNil(noName, "Empty name is invalid")
        
        let badRating = Meal(name: "Really bad rating", photo: nil, rating: -1)
        XCTAssertNotNil(badRating, "Negative ratings are invalid, be positive")
    }
}
